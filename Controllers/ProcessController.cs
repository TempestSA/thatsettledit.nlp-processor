using Microsoft.AspNetCore.Mvc;

using thatsettledit.nlp_processor.Infrastructure;
using thatsettledit.nlp_processor.Services;

namespace thatsettledit.nlp_processor.Controllers
{
    [Route("[controller]")]
    [AllowCrossSiteJson]
    public class ProcessController : Controller
    {
        private readonly INLPService nlpService;
        public ProcessController(INLPService _nlpService)
        {
            nlpService = _nlpService;
        }

        // GET api/values/5
        [HttpGet]
        public IActionResult Get()
        {
            return Json(nlpService.GetText("Why is venus the hottest planet?"));
        }

        // GET api/values/5
        [HttpGet("{text}")]
        public IActionResult Get(string text)
        {
            return Json(nlpService.GetText(text));
        }
    }
}
