﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using java.util;
using java.io;
using edu.stanford.nlp.pipeline;
using static edu.stanford.nlp.ling.CoreAnnotations;

namespace thatsettledit.nlp_processor.Services
{
    public interface INLPService
    {
        string GetText(string text);
    }

    public class NLPService : INLPService 
    {
        private StanfordCoreNLP _pipeline = null;

        public NLPService()
        {
            var jarRoot = @"C:\Programs\stanford-corenlp-full-2015-12-09\models";

            // Text for processing

            // Annotation pipeline configuration
            var props = new Properties();
            props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
            props.setProperty("ner.useSUTime", "0");

            // We should change current directory, so StanfordCoreNLP could find all the model files automatically
            var curDir = Environment.CurrentDirectory;
            Directory.SetCurrentDirectory(jarRoot);
            _pipeline = new StanfordCoreNLP(props);
            Directory.SetCurrentDirectory(curDir);
        }


        // Helper to get text processed for 
        public string GetText(string text)
        {
            // Path to the folder with models extracted from `stanford-corenlp-3.6.0-models.jar`
            var annotation = new Annotation(text);
            _pipeline.annotate(annotation);

            var sentences = annotation.get(typeof(TokensAnnotation));

            // Result - Pretty Print
            var result = "";
            using (var stream = new ByteArrayOutputStream())
            {
                _pipeline.jsonPrint(annotation, new PrintWriter(stream));
                result = stream.toString();
                stream.close();
            }
            return result;
        }
    }
}
