import React, { Component } from 'react';
import './App.css';
import ProcessText from './ProcessText'

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      input: 'I love cats',
      processedText: '',
      processed: {sentences: [ {index: 0, parse: "(ROOT\r\n  (S\r\n    (NP (PRP I))\r\n    (VP (VBP love)\r\n      (NP (NNS cats)))))"} ]}
    };
  }

  render() {
    return (
      <div className="App">
        <ProcessText state={this.state}></ProcessText>
      </div>
    );
  }
}

export default App;
