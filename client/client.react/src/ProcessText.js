import React, { Component } from 'react';
import fetch from 'isomorphic-fetch'

import './ProcessText.css';
import SentenceBreakdown from './SentenceBreakdown'

class ProcessText extends Component {

  constructor(props) {
    super(props)
    this.state = props.state
    // Bind callback methods to make `this` the correct context.
    this.onClickSubmit = this.onClickSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({input: event.target.value});
  }

  onClickSubmit(event) {
    let that = this;
    let inputText = this.state.input
    console.log(`Input = ${inputText}`)
    fetch("http://localhost:5000/process/" + inputText)
      .then(function(response) {
        if (!response.ok) {
          return
        } 
        return response.json().then(function(result) { 
          let res = JSON.parse(result)
          that.setState({
            processedText: result,
            processed: res
          });
          console.log(`Response = ${result}, ${res.sentences[0].parse}`)    
        });
      })  
  }

  render() {
    return (
      <div>
        <div className="TextEntry">
          <textarea className="input-text" value={this.state.input} onChange={this.handleChange}/>
        </div>
        <div>
          <button onClick={this.onClickSubmit}>Submit</button>
        </div>
        <div>
          {this.state.processed.sentences.map(function(s) {
            return <SentenceBreakdown className="sentence-breakdown" key={s.index || 0} state={s} ></SentenceBreakdown>
          })}
        </div>
      </div>
    );
  }
}

export default ProcessText;
