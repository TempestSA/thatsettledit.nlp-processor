import React, { Component } from 'react';

import './SentenceBreakdown.css';

class SentenceBreakdown extends Component {

  render() {
    return (
      <div className="sentence-breakdown">
        <ul>
          <li>parse: {this.props.state.parse}</li>
        </ul>
      </div>
    );
  }
}

export default SentenceBreakdown;
